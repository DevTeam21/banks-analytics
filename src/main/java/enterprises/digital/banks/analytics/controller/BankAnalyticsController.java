package enterprises.digital.banks.analytics.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import enterprises.digital.banks.analytics.domain.BankInfo;
import enterprises.digital.banks.analytics.service.BankAnalyticsService;

/*
 * Rest Controller for BankAnalytics
 * @author Lotchi
 *
 */

@RestController
@RequestMapping("/bankanalytics")
public class BankAnalyticsController {
	
	@Autowired
	private BankAnalyticsService service;
	
	
	/**
	* The app root
	* 
	*/
	@GetMapping("/info")
	public String getInfo() {
		return "Welcome to MyApp";
	}
	
	@GetMapping("/bankList")
	public List<BankInfo> getBankList() {
		System.out.println("### Getting all bankinfo");
		
		List<BankInfo> bankInfoList = service.getBankList();
		
		return bankInfoList;
		
	}
	
	@GetMapping("/bankSearch/{city}/{state}")
	public List<BankInfo> searchByCityAndState(@PathVariable String city, @PathVariable String state) {
		System.out.println("### Getting all bankinfo for the city=" + city  + ", state =" + state);
		
		List<BankInfo> bankInfoList = service.searchByCityAndState(city, state);
		
		return bankInfoList;
		
	}
	
	
	@GetMapping("/bankSearch/{attributes}")
	public List<BankInfo> searchByAttributes(@PathVariable String attributes) {
		System.out.println("### Getting all bankinfo by attributes " + attributes);
		String[] parts = attributes.split(",");
		
		List<BankInfo> bankInfoList = service.searchByAttribute(parts);
		
		return bankInfoList;
		
	}
	
	
}


