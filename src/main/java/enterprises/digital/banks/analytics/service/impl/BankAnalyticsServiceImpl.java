package enterprises.digital.banks.analytics.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import enterprises.digital.banks.analytics.domain.BankInfo;
import enterprises.digital.banks.analytics.service.BankAnalyticsService;
import enterprises.digital.banks.analytics.util.Util;


@Service
public class BankAnalyticsServiceImpl implements BankAnalyticsService {
	private List<BankInfo> bankInfoList = new ArrayList<BankInfo>();

	public BankAnalyticsServiceImpl() throws IOException {
		super();
		loadData();
	}

	@Override
	public List<BankInfo> searchByCityAndState(String city, String state) {

		List<BankInfo> resultList = new ArrayList<BankInfo>();

		for (BankInfo bankInfo : bankInfoList) {
			if ((bankInfo.getCity().equals(city)) && (bankInfo.getState().equals(state))) {
				resultList.add(bankInfo);
			}

		}

		return resultList;
	}

	@Override
	public List<BankInfo> searchByAttribute(String...attributes) {

		List<BankInfo> resultList = new ArrayList<BankInfo>();

		for (BankInfo bankInfo : bankInfoList) {
			
			for(int i = 0; i < attributes.length - 1; ++i) {
				String key   = attributes[i];
				String value = attributes[i+1];
				
				if((key.equals("Zipcode")) && (bankInfo.getZipcode().equals(value))) {
					resultList.add(bankInfo);
				}
				else if((key.equals("State")) && (bankInfo.getState().equals(value))) {
					resultList.add(bankInfo);
				}
				else if((key.equals("City")) && (bankInfo.getCity().equals(value))) {
					resultList.add(bankInfo);
				}
				else if((key.equals("Type")) && (bankInfo.getType().equals(value))) {
					resultList.add(bankInfo);
				}
				else if((key.equals("BankName")) && (bankInfo.getName().equals(value))) {
					resultList.add(bankInfo);
				}
				
			}
		
		}

		return resultList;
	}

	private void loadData() throws IOException {
		Resource resource = new ClassPathResource(Util.DATA_FILE_NAME);

		InputStream input = resource.getInputStream();

		File file = resource.getFile();

		BufferedReader reader = new BufferedReader(new FileReader(file));

		String line = null;
		int recordCount = 0;
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			if (line.length() == 0) {
				continue;
			}
			++recordCount;
			if (recordCount == 1) {
				continue; // header
			}
			String[] lineParts = line.split(",");
			Integer id = Integer.parseInt(lineParts[0].trim());
			String name = lineParts[1].trim();
			String type = lineParts[2].trim();
			String city = lineParts[3].trim();
			String state = lineParts[4].trim();
			String zipcode = lineParts[5].trim();

			BankInfo bankInfo = new BankInfo(id, name, type, city, state, zipcode);

			this.bankInfoList.add(bankInfo);

		}
		reader.close();
		input.close();
	}

	@Override
	public List<BankInfo> getBankList() {
		
		return this.bankInfoList;
	}

}
