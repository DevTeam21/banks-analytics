package enterprises.digital.banks.analytics.service;

import java.util.List;

import enterprises.digital.banks.analytics.domain.BankInfo;

public interface BankAnalyticsService {
	
	public List<BankInfo> getBankList();
	
	public List<BankInfo> searchByCityAndState(String city, String state);
	
	public List<BankInfo> searchByAttribute(String...attributes);
	
	

}
