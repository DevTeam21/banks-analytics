package enterprises.digital.banks.analytics;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class BankAnalytics {

    public static void main(String[] args) {
        SpringApplication.run(BankAnalytics.class, args);
    }
}
