package enterprises.digital.banks.analytics.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BankInfo implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private String type;
	private String city;
	private String state;
	private String zipcode;
	

	public BankInfo() {
		super();
	}
	
	

	public BankInfo(Integer id, String name, String type, String city, String state, String zipcode) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
	}
	

}
